<?php session_start(); include "core/core.php";
$vc = new curl_site();

if($_POST){

   	$fname = $_POST['first_name'];
	$lname = $_POST['last_name'];
	$ph = $_POST['phone'];
	$em = $_POST['email'];
    $gen = $_POST['gender'];
	$bdate = $_POST['birth_date'];
    $street = $_POST['street'];
    $addr2 = $_POST['address_additional'];
    $acity = $_POST['address_city'];
    $state = $_POST['address_province'];
    $zip = $_POST['address_zip'];
    $country = $_POST['address_country'];

    //$vc->setOptions("https://partner-gateway.mjplatform.com/v1/consumers?gender=male");
    $vc->setOptions("https://partner-gateway.mjplatform.com/v1/consumers");
    //$vc->setOptions("http://patients.greenpharms.com/headers.php");
    $obj = new stdClass();
    $obj->first_name = $fname;
    $obj->last_name = $lname;
    $obj->gender = $gen;
    $obj->active = 1;
    $obj->type = "medical";
    $obj->birth_date = $bdate;
    $obj->email_address = $em;
    $obj->addresses[0]= new stdClass;
    $obj->addresses[0]->street_address_1 = $street;
    $obj->addresses[0]->street_address_2 = $addr2;
    $obj->addresses[0]->city = $acity;
    $obj->addresses[0]->province_code = $state;
    $obj->addresses[0]->postal_code = $zip;
    $obj->phone_numbers[0] = new stdClass;
    $obj->phone_numbers[0]->number = $ph;
	
    $jcode = json_encode($obj);	
    
    $vc->postItems($jcode);
	$res = $vc->process();

    $gen = new general();
    $gen->redirect("success.html");exit;
}


?>


<!DOCTYPE html>
<head>

<title>
Patient Registration || Flagstaff
</title>


    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>    

    <link type="text/css" rel="stylesheet" href="stylesheet.css" />

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
   
    <script ="text/javascript">
    $('document').ready(function (){
        $('#edit-accept-agreement').click(function (){
            $('#hidden-field').toggle();
        })
        $('#bd').datepicker( {
            changeMonth:true,
            changeYear:true,
            yearRange : '1900:+0'});
    });
    </script> 
  
</head>

<div id="content-area" class="page">
    
    <div id="header" class="header">
    <img src="logo.png" class="img">
    <h1>GreenPharms Flagstaff Patient Agreement</h1>
        
    </div>

<form action="<?=SELF?>" accept-charset="UTF-8" method="post" id="mj-form" autocomplete="off">
    
<div class="agreement" id="agreement" style=font-size:20px>
    <h4>I acknowledge that by clicking "I Agree" at the bottom of this page, I am becoming a patient of Desertview Wellness & Healing Solutions DBA "GREENPHARMS". If I decide not to purchase marijuana or related products and services, there will be no charge. In the event that I do pay and elect to be a patient of Desertview Wellness & Healing Solutions DBA "GREENPHARMS", there will be NO REFUNDS <br><br>I acknowledge that marijuana, even if used for medical purposes, is illegal under Federal law and has been placed on Schedule 1 by the US FDA. As such, marijuana is considered to have no medical benefit and a significant potential for abuse. I assume responsibility for any violation of Federal law. Please read 1-9. By clicking "I Agree" on this page you understand that: <br><br>1. The medical director, staff, and representatives of Desertview Wellness & Healing Solutions DBA "GREENPHARMS" are addressing specifics aspects of my medical care and, unless otherwise stated, are in no way establishing themselves as my primary care physician/provider <br><br>2. I acknowledge that I am a resident of Arizona, I am at least 18 years of age, and I have not misrepresented any information to Desertview Wellness & Healing Solutions DBA "GREENPHARMS" <br><br>3. I acknowledge that I am not an agent of our law enforcement agency or state or federal government for the sole purpose of investigation <br><br>4. I acknowledge that I will not loiter or consume marijuana on the dispensary property (Per AZDHS Regulations) <br><br>5. I acknowledge that I am not recording any portion of my visit with Desertview Wellness & Healing Solutions DBA "GREENPHARMS" nor do I possess any recording equipment. I understand that Desertview Wellness & Healing Solutions DBA "GREENPHARMS" does not approve of such action. I further acknowledge that, without express written permission of Desertview Wellness & Healing Solutions DBA "GREENPHARMS", it is illegal to film or record in this office with a video camera, cell phone or any other recording devices; including still image or audio. Any such action is a direct violation of HIPPA regulations and patient/doctor confidentiality <br><br>6. I, (including my heirs or anyone acting on my behalf) by clicking agree at the bottom if this page, hold Desertview Wellness & Healing Solutions DBA "GREENPHARMS", the physician, agents, employees, and management harmless and release them from any liability resulting in any way whatsoever from my use of marijuana. This release of liability includes but is not limited to any bodily or psychological injury whether known or unknown as well as legal and/or employment problems resulting from my use of marijuana <br><br>7. I hereby authorize Desertview Wellness & Healing Solutions DBA "GREENPHARMS" to disclose, release, and verify my records as a patient to all legal parties for the purpose of obtaining marijuana to the extent provided by HIPAA laws and regulations. I understand that this authorization is valid for the period of time for which the Registry Card has been issued by ADHS and certified by my recommending physician <br><br>8. I hereby authorize the use and disclosure of my Desertview Wellness & Healing Solutions DBA "GREENPHARMS" patient records except for the personal identifying information, for use in data analysis of cannabis-treated patients <br><br>9. I hereby authorize Desertview Wellness & Healing Solutions DBA "GREENPHARMS" to disclose and verify my medical records to law enforcement should I be arrested or detained related to my possession of marijuana. I understand that this authorization is valid for the period of time for which the recommendation for marijuana has been issued by my referring physician</h4> <br><br><h2>Informed Consent and Release From Liability</h2><h4> <br>1. I have been evaluated by a physician and received a recommendation for medical marijuana. The physician made this recommendation based, in part, on the medical information I have provided. I have not misrepresented my medical condition in order to obtain my recommendation and it's my intent to use marijuana only as needed for the treatment of my medical condition, not for recreational or non medical purposes. I understand that it is my responsibility to be informed regarding state and federal laws regarding possession, use, sale, purchase. and/or distribution of marijuana <br><br>2. I must be an Arizona resident and over 18 years of age to obtain cannabis (medical marijuana) under Arizona law. If I am under 18 years of age, I must have a parent or guardian as my registered caregiver. <br><br>3. The federal government has classified marijuana as a Schedule 1 controlled substance. Schedule 1 substances are defined, in part, as having <br>A. A high potential for abuse; <br>B. No currently accepted medical use in treatment in the United States; and <br>C. A lack of accepted safety for use under medical supervision. Federal law prohibits the manufacture, distribution, and possession of marijuana even in states such as Arizona, which have modified their STATE LAWS to treat marijuana as medicine <br><br>4. Marijuana has not been approved by the Food and Drug Administration for marketing as a drug; therefore; the 'manufacture' of marijuana for medical use is NOT subject to any standards, quality control, or other oversight. Marijuana may contain unknown quantities of active ingredients(for example: can vary in potency), impurities, contaminants, and substances in addition to THC which is the primary psychoactive chemical component of marijuana <br><br>5. The use of marijuana can affect coordination, motor skills, and cognition (the ability to think, judge, and reason). While using marijuana, I know I should not drive, operate heavy machinery, or engage in any activities that require me to be alert and/or respond quickly. I understand that if I drive while under the influence of marijuana, I can be arrested for "driving under the influence" <br><br>6. Potential side effects from the use of marijuana include, but are not limited to, the following: dizziness, anxiety, confusion, sedation, low blood pressure, impairment of short-term memory, euphoria, difficulty in completing complex tasks, suppression of the body's immune system, inability to concentrate, impaired motor skills, paranoia, psychotic symptoms, general apathy, depression and/or restlessness. Marijuana may exacerbate schizophrenia in person predisposed to that disorder. In addition, the use of marijuana may increase eating, alter my perception of time and space, and impair my judgement <br><br>7. I understand that using marijuana while under the influence of alcohol is not recommended. Additional side effects may become present when using both alcohol and marijuana <br><br>8. I agree to contact 911 emergency services or my primary care physician if I experience any of the side effects listed above or if I become depressed or psychotic, have suicidal thoughts, experience crying spells, experience respiratory problems, changes in my normal sleeping patterns, extreme fatigue, increased irritability, or begin to withdraw from my family and/or friends <br><br>9. Smoking marijuana may cause respiratory problems and harm, including bronchitis, emphysema, and laryngitis. In the opinion of many researchers, marijuana smoke contains known carcinogens (chemicals that cause cancer) and smoking marijuana may increase the risk of respiratory disease and cancers in the lung, mouth, and tongue. In addition, marijuana smoke contains harmful chemicals known as tars. If I begin to experience respiratory problems while using marijuana, I will stop using it and report my symptoms to a physician <br><br>10. The risks, benefits, and drug interactions of marijuana are not fully understood. If I am taking medications or undergoing for any medical condition, I understand that I should consult with my treating physician(s) before using marijuana and that I should not discontinue any medication or treatment previously prescribed unless advised to do so by the treating physician(s) <br><br>11. Individuals may develop a tolerance to, and/or dependence on marijuana. I understand that if I require increasingly higher doses to achieve the same benefit or if I think that I may be developing a dependency on marijuana, I should contact a local substance abuse treatment center <br><br>12. Signs of withdrawal can include: feelings of depression, sadness, irritability, insomnia, restlessness, agitation, loss of appetite, trouble concentrating, sleep disturbances, and unusual tiredness <br><br>13. Symptoms of marijuana overdose include but are not limited to: anxiety attacks, anticipation, nausea, vomiting, hacking, coughing, disturbances in heart rhythms, and numbness in the hands, feet, arms, or legs. If I experience these symptoms, I agree to go to the nearest emergency room <br><br>14. If Desertview Wellness & Healing Solutions DBA "GREENPHARMS" subsequently learns that the information I have furnished is false or misleading, membership at the facility will not be granted and may be revoked indefinitely. I agree to promptly with Desertview Wellness & Healing Solutions DBA "GREENPHARMS" and/or provide additional information in the event of any inaccuracies or mistreatments in the information I have provided <br><br>15. I have had the opportunity to discuss the above matters (#1-#15) with the staff member(s) and to ask questions regarding anything I may not understand or that I believe need to be clarified. By clicking "I Agree" on this page I am acknowledging that I have read and fully understand this document and I Agree to all items listed within it</h4></div>   
    
<div class="form-item" id="edit-accept-agreement-wrapper">
<label class="option" for="edit-accept-agreement"><input type="checkbox" required name="accept_agreement" id="edit-accept-agreement" value="1" class="form-checkbox trigger-processed"> I Agree to All Terms</label>
</div>
    <br>  
<fieldset id="hidden-field" class="hidden-field" style="display:none;">
    
<div class="form-item" id="edit-first-name-wrapper">
<label for="edit-first-name">First Name<br><span class="form-required" title="This field is required."></span></label>
<input type="text" maxlength="128" name="first_name" id="edit-first-name" size="60" class="form-text" required>
</div>
    <br>
<div class="form-item" id="edit-last-name-wrapper">
<label for="edit-last-name">Last Name<br><span class="form-required" title="This field is required."></span></label>
<input type="text" maxlength="128" name="last_name" id="edit-last-name" size="60" class="form-text" required>
</div>
    <br>
<div class="form-item" id="edit-gender-wrapper"><label for="edit-gender">Gender<br></label><select id="edit-gender" name="gender" class="addresses-gender" maxlength="16"><option value="male">Male</option><option value="female">Female</option><option value="unspecified">Unspecified</option></option></select></div>
    <br>
<label for="birth_date">Birthday<br></label><input type="text" name="birth_date" id="bd" required>
    <br>
    <br>    
<div class="form-item" id="edit-phone-wrapper">
<label for="edit-phone">Phone Number<br><span class="form-required" title="This field is required."></span></label>
<input type="tel" pattern="([0-9]{3}-[0-9]{3}-[0-9]{4})|([0-9]{3}.[0-9]{3}.[0-9]{4})|([0-9]{3}[0-9]{3}[0-9]{4})" maxlength="10" name="phone" id="edit-phone" size="60" class="form-text" required>
</div>
    <br>
<div class="form-item" id="edit-email-wrapper">
<label for="edit-email">E-mail<br><span class="form-required" title="This field is required."></span></label>
<input type="email" maxlength="128" name="email" id="edit-email" size="60" class="form-text">
</div>
    <br>
<div class="addresses-form">
<div class="form-item" id="edit-street-wrapper">
<label for="edit-street">Mailing Address<br></label>
<input type="text" maxlength="255" name="street" id="edit-street" size="60" class="form-text">
</div>
    <br>
<div class="form-item" id="edit-additional-wrapper">
<label for="edit-additional">Apt/Suite#<br></label>
<input type="text" maxlength="255" name="address_additional" id="edit-additional" size="60" class="form-text">
</div>
    <br>
<div class="form-item" id="edit-address-wrapper">
<label for="edit-city">City<br></label>
<input type="text" maxlength="255" name="address_city" id="edit-city" size="60" class="form-text">
</div>
    <br>
<div class="form-item" id="edit-province-wrapper"><label for="edit-province">State / Province<br></label><select id="edit-province" name="address_province" class="addresses-province-field" maxlength="16"><option value="AL">Alabama</option><option value="AK">Alaska</option><option value="AZ" selected="selected">Arizona</option><option value="AR">Arkansas</option><option value="CA">California</option><option value="CO">Colorado</option><option value="CT">Connecticut</option><option value="DE">Delaware</option><option value="DC">District Of Columbia</option><option value="FL">Florida</option><option value="GA">Georgia</option><option value="HI">Hawaii</option><option value="ID">Idaho</option><option value="IL">Illinois</option><option value="IN">Indiana</option><option value="IA">Iowa</option><option value="KS">Kansas</option><option value="KY">Kentucky</option><option value="LA">Louisiana</option><option value="ME">Maine</option><option value="MD">Maryland</option><option value="MA">Massachusetts</option><option value="MI">Michigan</option><option value="MN">Minnesota</option><option value="MS">Mississippi</option><option value="MO">Missouri</option><option value="MT">Montana</option><option value="NE">Nebraska</option><option value="NV">Nevada</option><option value="NH">New Hampshire</option><option value="NJ">New Jersey</option><option value="NM">New Mexico</option><option value="NY">New York</option><option value="NC">North Carolina</option><option value="ND">North Dakota</option><option value="OH">Ohio</option><option value="OK">Oklahoma</option><option value="OR">Oregon</option><option value="PA">Pennsylvania</option><option value="RI">Rhode Island</option><option value="SC">South Carolina</option><option value="SD">South Dakota</option><option value="TN">Tennessee</option><option value="TX">Texas</option><option value="UT">Utah</option><option value="VT">Vermont</option><option value="VA">Virginia</option><option value="WA">Washington</option><option value="WV">West Virginia</option><option value="WI">Wisconsin</option><option value="WY">Wyoming</option><option value="AA">Armed Forces Americas</option><option value="AE">Armed Forces Europe</option><option value="AP">Armed Forces Pacific</option><option value="AS">American Samoa</option><option value="FM">Federated States of Micronesia</option><option value="GU">Guam</option><option value="MH">Marshall Islands</option><option value="MP">Northern Mariana Islands</option><option value="PW">Palau</option><option value="PR">Puerto Rico</option><option value="VI">Virgin Islands</option></select></div>
    <br>
<div class="form-item" id="edit-zip-wrapper">
<label for="edit-zip">Postal Code<br></label>
<input type="text" maxlength="5" name="address_zip" id="edit-zip" class="form-text">
</div>
    <br>
<div class="form-item" id="edit-country-wrapper">
<label for="edit-country">Country<br></label>
<select name="address_country" class="form-zip" id="edit-country"><option value=""></option><option value="ca">Canada</option><option value="es">Spain</option><option value="us" selected="selected">United States</option></select>
</div>
</div>
    <br>
<input type="submit" name="op" id="edit-register" value="Register!" class="form-submit">
</fieldset>
    
</div>
</form>
    
</div>

</html>
