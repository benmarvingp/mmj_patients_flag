<?php
class db{
	var $dbhandle;
	var $host,$user,$pass,$dbname;
	var $connectcount, $reconnect_forever;

	public function __construct($host,$user,$pass,$dbname){
		$this->host = $host;$this->user = $user;$this->pass = $pass;$this->dbname = $dbname;
		$this->connectcount++;
		$this->open_connection($host,$user,$pass,$dbname);
		if(strpos($_SERVER['PHP_SELF'], 'cronjobs')){
			$this->reconnect_forever = TRUE;
		}
	}

	public function open_connection($host,$user,$pass,$dbname){
		$this->dbhandle = mysqli_connect($host,$user,$pass);
		if($this->dbhandle==true){
			if(mysqli_select_db($this->dbhandle,$dbname)===FALSE){
				@mysqli_error($this->dbhandle);
				die("Invalid domain name or read databases.");
			}
		}else{
			@mysqli_error($this->dbhandle);
			die("Invalid domain name or read databases.");
		}
	}


	public function search_db($query, $forcemasterread=NULL){
		if($this->reconnect_forever){
			mysqli_ping($this->dbhandle);
		}			
		@mysqli_set_charset($this->dbhandle, "utf8mb4");
		$res = mysqli_query($this->dbhandle,$query);

		if (!$res) {
			// $this->db_return_info(mysqli_error($this->dbhandle));
			die('Could not execute query:' . mysqli_error($this->dbhandle));
			return false;
		}
		while($row=mysqli_fetch_object($res)){
			$object_array[] = $row;
		}
		mysqli_free_result($res);
		if(@$object_array)return $object_array;
		else return NULL;
	}

	public function execute($query){
		if($this->reconnect_forever){
			// Checks connection and reconnects if needed
			mysqli_ping($this->dbhandle);
		}			

		if(mysqli_query($this->dbhandle,$query)){
			if(strtoupper(substr($query, 0, 6)) == 'UPDATE'){
				return mysqli_affected_rows($this->dbhandle);
			}else{
				return mysqli_insert_id($this->dbhandle);
			}
		}else{
			if (mysqli_errno($this->dbhandle) == 1062) {
				return mysqli_error($this->dbhandle);
			}else{
				// $this->db_return_info(mysqli_error($this->dbhandle));
				die('Could not execute query:' . mysqli_error($this->dbhandle));
			}
		}
	}

	public function close_db(){
		if($this->dbhandle){
			mysqli_close($this->dbhandle);
		}
		if($this->readdbhandle){
			mysqli_close($this->readdbhandle);
		}
	}
	
	function db_return_info($mysqlerror=NULL){
		if($this->reconnect_forever){
			return false;
		}
		$this->error = TRUE;
		$this->error_message = "Service Unavailable";
		if($mysqlerror){
			$this->error_message = "$mysqlerror";
		}
		$manufacturer = $_POST['manufacturer'];
		// Wrap results into a json object and print it
		$json = new StdClass;
		if($this->error){
			if(strpos($this->error_message, 'Error') === 0){
				$this->log_error();
			}
			if($this->status){
				$json->status = $this->status;
			}else{
				$json->status = 'error';
			}
			$json->message = "$this->error_message";
		}else{
			$json->status = 'success';
			if($this->message){
				$json->message = $this->message;
			}else{
				$json->message = 'ok';
			}
			if($this->data){
				$json->data = $this->data;
			}else{
				if($manufacturer && strtolower($manufacturer) != 'apple'){
					$json->data = array();
				}
			}
		}

		$data = json_encode($json);
		if($manufacturer && strtolower($manufacturer) != 'apple'){
			// Find any nulls in the json object and remove the keys
			$data = preg_replace('/,\s*"[^"]+":null|"[^"]+":null,?/', '', $data);
		}
		$key = md5('jerold @ rentershelpdesk');
		$result = $this->db_aes256Encrypt($key, $data);
		/*if($_POST['build'] >= 11){
			$result = base64_encode($result);
		}*/
		echo $result;
		die;
	}

	function db_aes256Encrypt($key, $data) {
		$padding = 16 - (strlen($data) % 16);
		$data .= str_repeat(chr($padding), $padding);
		return mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_CBC, str_repeat("\0", 16));
	}



}