<?php
/*
The following product is the intellectual property of Jerold Dodds and AWeb L.L.C.
Unauthorized usage of program code and files is strictly prohibited without the 
explicitly expressed written consent of its author. Usage is defined as the 
Copying, Modification, Inclusion and/or Distribution in part or as whole.
For more information regarding code Licensing please consult: http://www.a-webco.net/
*/

//mailer class, use this class to write emails
class mailer{
	var $subject;
	var $message;
	var $recipient;
	var $sender;
	
	//this class can accept multiple recipients
	//the recipient array must be built prior to constructing the class
	function mailer($sender,$recipient,$subject,$message){
		if(is_array($recipient))
			$recipient = implode(",",$recipient);
		$this->sender=$sender;
		$this->sender;
		$this->recipient=$recipient;
		$this->subject=$subject;
		$this->message=$message;
	}
	function send(){
		$extra_headers = "From:$this->sender\r\nContent-type: text/plain\r\nX-mailer: PHP/" . phpversion();
		mail("$this->recipient","$this->subject","$this->message", $extra_headers);
	}
	function disp(){
		echo $this->subject, $this->message, $this->recipient ,$this->sender;
	}
}
