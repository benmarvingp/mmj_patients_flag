<?php
Class __superClass{
	var $dbc;
        var $gen_functions;

	public function __construct(){
		$this->create_connection();
                $this->load_modules();
	}
	
        public function create_connection(){
                //$dbc = new db("localhost","root","phsmdyVHabpRZ","rhd");
                $dbc = new db("internal-db.s97434.gridserver.com","db97434_wood","1*bkZ-7lC}g","db97434_competition");
                $this->dbc = $dbc;
        }
        public function load_modules(){
                $this->gen_functions = new general();
        }
}
Class __controller extends __superClass{
        protected $model,$controller_name,$action;
        var $error;

        public function __construct($model=NULL,$controller_name=NULL, $action=NULL){
                parent::__construct();

                $this->model = $model;$this->controller_name = $controller_name;$this->action = $action;
                $_POST = $this->sanitize_input($_POST);
                $_GET = $this->sanitize_input($_GET);
                $_REQUEST = $this->sanitize_input($_REQUEST);              

                return $this->loadController();
        }
        public function sanitize_input($input=NULL){
                if(is_array($input))foreach($input as $key=>$val)$input[$key]=$this->sanitize_input($input[$key]);
                else{
                        $input = $this->dbc->dbhandle->real_escape_string($input);
                }
                if($input)return $input;
        }
        public function loadController(){
                $controller_name = $this->controller_name;

                if(realpath($rq = DOCROOT."/controllers/".$controller_name."."."php")) {
                        require_once $rq;
                } else {
                        $this->error = "Controller not found.";
                }
                $this->controller = new $controller_name();
                //return $controller_handle;
        }
}
