<?php
/*
The following product is the intellectual property of Jerold Dodds and AWeb L.L.C.
Unauthorized usage of program code and files is strictly prohibited without the 
explicitly expressed written consent of its author. Usage is defined as the 
Copying, Modification, Inclusion and/or Distribution in part or as whole.
For more information regarding code Licensing please consult: http://www.a-webco.net/
*/
class curl_site{
	var $ch;
	var $hoststring;
	
	function __construct(){
		$this->ch = curl_init();
	}
	function process(){
		$result = curl_exec($this->ch);
		curl_close($this->ch);	
		return $result;
	}
	function setOptions($hostString){
		//condition URL for properness
		$iheader = array('Content-Type: application/json','Accept: application/json','x-mjf-api-key: 448535eb-890f-11e8-89d1-06f060abae92','x-mjf-organization-id: 763','x-mjf-facility-id: 1713','x-mjf-user-id: 7644');

		$this->header = $iheader;

		curl_setopt($this->ch, CURLOPT_HEADER, 1);
		curl_setopt($this->ch, CURLOPT_HTTPHEADER, $iheader);

		if(isset($hostString)){
	        $this->hoststring = $hostString;
	        curl_setopt($this->ch, CURLOPT_URL, $this->hoststring);
		}
		curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
	}
	function postItems($item=NULL){
		curl_setopt ($this->ch, CURLOPT_POST, 1);
		if($item)curl_setopt ($this->ch, CURLOPT_POSTFIELDS, $item);
		curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
	}

	function setUser($user_name,$password){
		curl_setopt ($this->ch, CURLOPT_USERPWD, "$user_name:$password");
	}
}	
