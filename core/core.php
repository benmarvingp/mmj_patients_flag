<?php
if (PHP_SAPI != 'cli')define('DOCROOT',$_SERVER['DOCUMENT_ROOT']);
else define('DOCROOT','/var/www/patients.greenpharms.com/site/');

//if (PHP_SAPI != 'cli')define('DOCROOT','/var/www/development/vegas_unlocked/version3/vegas-unlocked/site');
//else define('DOCROOT','/var/www/vegasunlocked.com/site');


define('BASEURL',"https://".$_SERVER['SERVER_NAME']."/");
define ('SELF', $_SERVER['PHP_SELF']);

//define('DOMAIN',"");

// require_once(DOCROOT."/core/modules/super_class.php");
// require_once(DOCROOT."/core/modules/user.php");
// require_once(DOCROOT."/core/modules/properties.php");
// require_once(DOCROOT."/core/modules/mysqli_db_class.php");
// require_once(DOCROOT."/core/plugins/PHPMailer/class.phpmailer.php");

spl_autoload_extensions('.php');
spl_autoload_register('custom_autoloader');

function custom_autoloader(){
	$module_path = DOCROOT."/core/modules/";
	$plugin_path = DOCROOT."/core/plugins/";
	require_once(DOCROOT."/core/plugins/general-functions.php");
	
	$x = new general();
	$module_files = $x->retrieve_path_files($module_path);
	$plugin_files = $x->retrieve_path_files($plugin_path);
	$all_includes = array_merge($module_files,$plugin_files);

	foreach($all_includes as $class_file){
		require_once($class_file['dirname']."/".$class_file['basename']);
	}
}
