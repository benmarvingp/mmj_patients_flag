<?php 
class general{
    public function __construct(){

    }
    function redirect($url) {
        if(!headers_sent()) {
            //If headers not sent yet... then do php redirect
            header('Location: '.$url);
            exit;
        } else {
            //If headers are sent... do javascript redirect... if javascript disabled, do html redirect.
            echo '<script type="text/javascript">';
            echo 'window.location.href="'.$url.'";';
            echo '</script>';
            echo '<noscript>';
            echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
            echo '</noscript>';
            exit;
        }
    }
    public function retrieve_path_files($path){
        $filename = '';
        if (is_dir($path)) {
            if ($handle = opendir($path)) {
                while($file=readdir($handle)){
                    $file_data = pathinfo($file);
                    if($file_data['basename']=="."||$file_data['basename']=="..")continue;
                    if(is_dir($path.$file)){
                        //var_dump($files);
                        $files = array_merge($this->retrieve_path_files($path.$file));
                    }
                    else{
                        $file_data['dirname'] = $path;
                        $files[] = $file_data;
                    }
                }
            }
        }
        return $files;
    }
    function analytics(){
        echo "<script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-32115086-1', 'auto');
          ga('send', 'pageview');

        </script>";
    }
}
